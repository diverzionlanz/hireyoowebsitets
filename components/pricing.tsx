export default function Pricing(){
    return (
        <section id="pricing" className="pricing-section pt-120 pb-120">
			<div className="container">
				<div className="row justify-content-center">
					<div className="col-xxl-5 col-xl-6 col-lg-8 col-md-9">
						<div className="section-title text-center mb-35">
							<h1 className="mb-25 wow fadeInUp" data-wow-delay=".2s">Choose a Plan</h1>
							<p className="wow fadeInUp" data-wow-delay=".4s">Lorem ipsum dolor sit amet consetetur sadipscing elitr sswed diam nonumy eirmod tempor nvidunt.</p>
						</div>
					</div>
				</div>

				<div className="pricing-nav-wrapper mb-60">
					<ul className="nav nav-pills" id="pills-tab" role="tablist">
						<li role="presentation">
							<a className="active" id="pills-month-tab" data-toggle="pill" href="#pills-month" role="tab" aria-controls="pills-month" aria-selected="true">Monthly</a>
						</li>
						<li role="presentation">
							<a id="pills-year-tab" data-toggle="pill" href="#pills-year" role="tab" aria-controls="pills-year" aria-selected="false">Yearly</a>
						</li>
					</ul>
				</div>

				<div className="tab-content" id="pills-tabContent">
					<div className="tab-pane fade show active" id="pills-month" role="tabpanel" aria-labelledby="pills-month-tab">
						<div className="row justify-content-center">
							<div className="col-lg-4 col-md-8 col-sm-10">
								<div className="single-pricing">
									<div className="pricing-header">
										<h1 className="price">$36</h1>
										<h3 className="package-name">Basic Account</h3>
									</div>
									<div className="content">
										<ul>
											<li> <i className="lni lni-checkmark active"></i> Unlimited Acces</li>
											<li> <i className="lni lni-checkmark active"></i> Single User </li>
											<li> <i className="lni lni-checkmark active"></i> Unlimited Storage</li>
											<li> <i className="lni lni-close"></i> 24/7 Support </li>
											<li> <i className="lni lni-close"></i> Free Future Updates</li>
										</ul>
									</div>
									<div className="pricing-btn">
										<a href="javascript:void(0)" className="main-btn btn-hover border-btn">Get Start</a>
									</div>
								</div>
							</div>
							<div className="col-lg-4 col-md-8 col-sm-10">
								<div className="single-pricing">
									<div className="pricing-header">
										<h1 className="price">$56</h1>
										<h3 className="package-name">Standard Account</h3>
									</div>
									<div className="content">
										<ul>
											<li> <i className="lni lni-checkmark active"></i> Unlimited Acces</li>
											<li> <i className="lni lni-checkmark active"></i> 20+ Users </li>
											<li> <i className="lni lni-checkmark active"></i> Unlimited Storage</li>
											<li> <i className="lni lni-checkmark active"></i> 24/7 Support </li>
											<li> <i className="lni lni-checkmark active"></i> Free Future Updates</li>
										</ul>
									</div>
									<div className="pricing-btn">
										<a href="javascript:void(0)" className="main-btn btn-hover">Get Start</a>
									</div>
								</div>
							</div>
							<div className="col-lg-4 col-md-8 col-sm-10">
								<div className="single-pricing">
									<div className="pricing-header">
										<h1 className="price">$89</h1>
										<h3 className="package-name">Premium Account</h3>
									</div>
									<div className="content">
										<ul>
											<li> <i className="lni lni-checkmark active"></i> Unlimited Acces</li>
											<li> <i className="lni lni-checkmark active"></i> Unlimited Users </li>
											<li> <i className="lni lni-checkmark active"></i> Unlimited Storage</li>
											<li> <i className="lni lni-checkmark active"></i> 24/7 Support </li>
											<li> <i className="lni lni-checkmark active"></i> Free Future Updates</li>
										</ul>
									</div>
									<div className="pricing-btn">
										<a href="javascript:void(0)" className="main-btn btn-hover border-btn">Get Start</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div className="tab-pane fade" id="pills-year" role="tabpanel" aria-labelledby="pills-year-tab">
						<div className="row justify-content-center">
							<div className="col-lg-4 col-md-8 col-sm-10">
								<div className="single-pricing">
									<div className="pricing-header">
										<h1 className="price">$136</h1>
										<h3 className="package-name">Basic Account</h3>
									</div>
									<div className="content">
										<ul>
											<li> <i className="lni lni-checkmark active"></i> Unlimited Acces</li>
											<li> <i className="lni lni-checkmark active"></i> Single User </li>
											<li> <i className="lni lni-checkmark active"></i> Unlimited Storage</li>
											<li> <i className="lni lni-close"></i> 24/7 Support </li>
											<li> <i className="lni lni-close"></i> Free Future Updates</li>
										</ul>
									</div>
									<div className="pricing-btn">
										<a href="javascript:void(0)" className="main-btn btn-hover border-btn">Get Start</a>
									</div>
								</div>
							</div>
							<div className="col-lg-4 col-md-8 col-sm-10">
								<div className="single-pricing">
									<div className="pricing-header">
										<h1 className="price">$156</h1>
										<h3 className="package-name">Standard Account</h3>
									</div>
									<div className="content">
										<ul>
											<li> <i className="lni lni-checkmark active"></i> Unlimited Acces</li>
											<li> <i className="lni lni-checkmark active"></i> 20+ Users </li>
											<li> <i className="lni lni-checkmark active"></i> Unlimited Storage</li>
											<li> <i className="lni lni-checkmark active"></i> 24/7 Support </li>
											<li> <i className="lni lni-close"></i> Free Future Updates</li>
										</ul>
									</div>
									<div className="pricing-btn">
										<a href="javascript:void(0)" className="main-btn btn-hover">Get Start</a>
									</div>
								</div>
							</div>
							<div className="col-lg-4 col-md-8 col-sm-10">
								<div className="single-pricing">
									<div className="pricing-header">
										<h1 className="price">$189</h1>
										<h3 className="package-name">Premium Account</h3>
									</div>
									<div className="content">
										<ul>
											<li> <i className="lni lni-checkmark active"></i> Unlimited Acces</li>
											<li> <i className="lni lni-checkmark active"></i> Unlimited Users </li>
											<li> <i className="lni lni-checkmark active"></i> Unlimited Storage</li>
											<li> <i className="lni lni-checkmark active"></i> 24/7 Support </li>
											<li> <i className="lni lni-checkmark active"></i> Free Future Updates</li>
										</ul>
									</div>
									<div className="pricing-btn">
										<a href="javascript:void(0)" className="main-btn btn-hover border-btn">Get Start</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
    );
}