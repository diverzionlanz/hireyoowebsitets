export default function Subscribe() {
    return (
        <section id="contact" className="subscribe-section pt-120">
			<div className="container">
				<div className="subscribe-wrapper img-bg">
					<div className="row align-items-center">
						<div className="col-xl-6 col-lg-7">
							<div className="section-title mb-15">
								<h1 className="text-white mb-25">Subscribe Our Newsletter</h1>
								<p className="text-white pr-5">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor</p>
							</div>
						</div>
						<div className="col-xl-6 col-lg-5">
							<form action="#" className="subscribe-form">
								<input type="email" name="subs-email" id="subs-email" placeholder="Your Email" />
								<button type="submit" className="main-btn btn-hover">Subscribe</button>
							</form>
						</div>
					</div>
				</div>
			</div>
		</section>
    );
}