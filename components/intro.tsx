
export default function Intro(){
    return (
        <section id="home" className="hero-section">
            <div className="container">
                <div className="row align-items-center position-relative">
                    <div className="col-lg-6">
                        <div className="hero-content">
                            <h1 className="wow fadeInUp" data-wow-delay=".4s">Launch your Hireyoo <br/> Webapp anytime!</h1>
                                <p className="wow fadeInUp" data-wow-delay=".6s">
                                    Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.
                                </p>
							    <a href="javascript:void(0)" className="main-btn border-btn btn-hover wow fadeInUp" data-wow-delay=".6s">Get Started</a>
							    <a href="#features" className="scroll-bottom"> <i className="lni lni-arrow-down"></i></a>
                        </div>
					</div>
					<div className="col-lg-6">
						<div className="hero-img wow fadeInUp" data-wow-delay=".5s">
							<img src="assets/img/hero/hero-img.png" alt="" />
						</div>
					</div>
                </div>
			</div>
        </section>
    );
}