export default function header(){
   return (
    <section className="header">
    <div className="navbar-area">
        <div className="container">
             <div className="row align-items-center">
                 <div className="col-lg-12">
                      <nav className="navbar navbar-expand-lg">
                          <a className="navbar-brand" href="#">
                              <img src="../../assets/img/logo/logo.svg" alt="Logo" />
                          </a>
                          <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                              <span className="toggler-icon"></span>
                              <span className="toggler-icon"></span>
                              <span className="toggler-icon"></span>
                          </button>
                          <div className="collapse navbar-collapse sub-menu-bar" id="navbarSupportedContent">
                              <ul id="nav" className="navbar-nav ml-auto">
                                <li className="nav-item">
                                  <a className="page-scroll" href="#home">Home</a>
                                </li>
                                <li className="nav-item">
                                  <a className="page-scroll" href="#features">Features</a>
                                </li>
                                <li className="nav-item">
                                  <a className="page-scroll" href="#about">About</a>
                                </li>

                                {/* <li className="nav-item">
                                  <a className="page-scroll" href="#why">Why</a>
                                </li> */}
                                <li className="nav-item">
                                  <a className="page-scroll" href="#pricing">Pricing</a>
                                </li>
                                <li className="nav-item">
                                  <a className="page-scroll" href="#testimonials">Clients</a>
                                </li>
                              </ul>
                          </div>
                      </nav>
                 </div>
             </div>
        </div>
    </div>

</section>
   );
}