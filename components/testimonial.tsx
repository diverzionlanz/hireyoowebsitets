export default function Testimonial() {
    return (
        <section id="testimonials" className="testimonial-section">
			<div className="container">
				<div className="section-title text-center">
					<h1 className="mb-30">What our customers says</h1>
				</div>
				<div className="testimonial-active-wrapper">
                    <div className="shapes">
						<img src="/../../assets/img/about/about-left-shape.svg" alt="" className="shape shape-1" />
						<img src="/../../assets/img/about/left-dots.svg" alt="" className="shape shape-2" />
					</div>
                    <div className="testimonial-active">
                        <div className="single-testimonial">
							<div className="row">
								<div className="col-xl-5 col-lg-5">
									<div className="testimonial-img">
										<img src="../../assets/img/testimonial/testimonial-1.png" alt="" />
										<div className="quote">
											<i className="lni lni-quotation"></i>
										</div>
									</div>
								</div>
			
								<div className="col-xl-6 offset-xl-1 col-lg-6 offset-lg-1">
									<div className="content-wrapper">
										<div className="content">
											<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed dinonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem.</p>
										</div>
										<div className="info">
											<h4>Jonathon Smith</h4>
											<p>Developer and Youtuber</p>
										</div>
									</div>
								</div>
							</div>
						</div>

				
						<div className="single-testimonial">
							<div className="row">
								<div className="col-xl-5">
									<div className="testimonial-img">
										<img src="../../assets/img/testimonial/testimonial-2.png" alt="" />
										<div className="quote">
											<i className="lni lni-quotation"></i>
										</div>
									</div>
								</div>
			
								<div className="col-xl-6 offset-xl-1">
									<div className="content-wrapper">
										<div className="content">
											<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed dinonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem.</p>
										</div>
										<div className="info">
											<h4>Gray Simon</h4>
											<p>UIX Designer and Developer</p>
										</div>
									</div>
								</div>
							</div>
						</div>

				
						<div className="single-testimonial">
							<div className="row">
								<div className="col-xl-5">
									<div className="testimonial-img">
										<img src="../../assets/img/testimonial/testimonial-3.png" alt=""/>
										<div className="quote">
											<i className="lni lni-quotation"></i>
										</div>
									</div>
								</div>
			
								<div className="col-xl-6 offset-xl-1">
									<div className="content-wrapper">
										<div className="content">
											<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed dinonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem.</p>
										</div>
										<div className="info">
											<h4>Michel Smith</h4>
											<p>Traveler and Vloger</p>
										</div>
									</div>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
		</section>
    );
}