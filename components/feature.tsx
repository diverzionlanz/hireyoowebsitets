export default function Feature() {
   return (
    <section id="features" className="feature-section pt-120">
        <div className="container">
            <div className="row justify-content-center">
                <div className="col-lg-4 col-md-8 col-sm-10">
                    <div className="single-feature">
                        <div className="icon">
                            <i className="lni lni-bootstrap"></i>
                            {/* <img src="/../../assets/img/feature/icon1.svg" alt="" className="shape shape-2" /> */}
                        </div>
                        <div className="content">
                            <h3>Bootstrap 5</h3>
                            <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore</p>
                       </div>
                    </div>
                </div>
                <div className="col-lg-4 col-md-8 col-sm-10">
                    <div className="single-feature">
                        <div className="icon">
                            <i className="lni lni-layout"></i>
                            {/* <img src="/../../assets/img/feature/icon2.svg" alt="" className="shape shape-2" /> */}
                        </div>
                        <div className="content">
                            <h3>Clean Design</h3>
                            <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore</p>
                        </div>
                    </div>
                </div>
                <div className="col-lg-4 col-md-8 col-sm-10">
                    <div className="single-feature">
                        <div className="icon">
                            <i className="lni lni-coffee-cup"></i>
                        </div>
                        <div className="content">
                            <h3>Easy to Use</h3>
                            <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
   );
}